The Track'n Stack module allows users to create collaborative pieces.
See http://drupal.org/project/tracknstack for more information.

Requirements
------------

This module requires Drupal 6.xx.

Recommended
-----------

1) SoX (http://sox.sourceforge.net) to validate submitted files and mix
   them automatically.
2) jPlayer javascript library (http://www.jplayer.org) to play your mixdown
   in a fancy HTML5 player (version 2.0.0 and up). Get also the CSS blue
   monday skin. You may need to tweak the style sheets a little to make it
   look pretty.

Installation
------------

1) Copy the tracknstack folder in the sites/all/modules subdirectory of your
   Drupal installation.
2) Enable the tracknstack module in Administer -> Site building -> Modules.

Configuration
-------------

1) Configure settings in Administer -> Site configuration -> Track'n Stack.
2) Assign user permissions in Administer -> User management -> Access control
   under the "tracknstack module" heading.
3) Run cron as an authenticated user (see http://drupal.org/node/23714).

Contributing
------------
You can visit http://drupal.org/project/issues/tracknstack to post issues.

--------------------------------------------------------------------------------
Philippe Bergeron                       pb@freakywaves.com | tns.freakywaves.com

