<?php

/**
 * Format the teaser for a tracknstack node.
 */
function theme_tracknstack_teaser($node) {
  $items = array();

  if (user_access('access tracknstack')) {
    $mixdown_link = '';

    if ($node->tracknstack->mixdown_fid) {
      $file = tracknstack_file_load($node->tracknstack->mixdown_fid);
      $file_extension = tracknstack_get_filename_extension($file->filename);
      $mixdown_url = tracknstack_get_base_url() . '?q=tracknstack/mixdownload/' . $node->nid;
      $mixdown_link = l($file_extension, $mixdown_url, array('attributes' => array('title' => t('Download mixdown.'))));
    }

    if ($mixdown_link) {
      $items[] = '<strong>' . t('Mixdown') . ':</strong> ' . $mixdown_link;
    } else if (tracknstack_get_absolute_sox_path()) {
      $items[] = t('Processing mixdown...');
    }
  }

  if ($node->tracknstack->tempo) {
    $items[] = '<strong>' . t('Tempo') . ':</strong> ' . $node->tracknstack->tempo . ' ' . t('BPM');
  }

  if ($node->tracknstack->beats_per_measure && $node->tracknstack->beat_value) {
    $items[] = '<strong>' . t('Time signature') . ':</strong> ' . $node->tracknstack->beats_per_measure . '/' . $node->tracknstack->beat_value;
  }

  if ($node->tracknstack->main_key) {
    $items[] = '<strong>' . t('Main key') . ':</strong> ' . $node->tracknstack->main_key;
  }

  $output = tracknstack_embedded_player($node);
  $output .= theme('item_list', $items);

  return $output;
}

/**
 * Format the tracknstack node for display
 */
function theme_tracknstack_display($node) {
  global $user;

  $output = theme('tracknstack_teaser', $node);

  if ($node->tracknstack_checkin) {
    $cicount = sizeof($node->tracknstack_checkin);

    $is_view_allowed = $cicount > 0;

    if ($is_view_allowed) {
      $headers = array(t('Checkin Date'), t('Username'), t('Files'), '');

      $rows = array();

      $is_delete_allowed = $user->uid == $node->uid || user_access('delete tracknstack files');

      $actions_delete_html = '<img src="';
      $actions_delete_html .= base_path() . drupal_get_path("module", "tracknstack") . '/images/delete.png';
      $actions_delete_html .= '" title="';
      $actions_delete_html .= t('Delete file.');
      $actions_delete_html .= '"/>';

      $base_url = tracknstack_get_base_url();

      for ($i = $cicount - 1; $checkin = $node->tracknstack_checkin[$i]; $i--) {
        $ciuser = tracknstack_get_user($checkin->uid);

        $cifilecount = sizeof($checkin->files);
        $is_download_allowed = user_access('download tracknstack files');

        $files_html = '';
        $actions_html = '';

        for ($j = $cifilecount - 1; $file = $checkin->files[$j]; $j--) {
          $filename = check_plain(tracknstack_get_filename($file, $node));

          if ($is_download_allowed || $checkin->uid == $user->uid) {
            $files_html .= l($filename, $base_url . '?q=tracknstack/download/' . $file->fid, array('attributes' => array('title' => t('Download file.'))));
          } else {
            $files_html .= $filename;
          }

          $files_html .= '<br/>';

          if ($is_delete_allowed || $checkin->uid == $user->uid) {
            $actions_html .= l($actions_delete_html, $base_url . '?q=tracknstack/delete_file/' . $file->fid, array('html' => true));
            $actions_html .= '<br/>';
          }
        }

        $row_values = array(format_date($checkin->datein), theme('username', $ciuser), $files_html, $actions_html);

        $rows[] = array(
          'data' => $row_values,
          'style' => 'vertical-align:top'
        );
      }
    }

    $output .= theme_table($headers, $rows);
  }

  return $output;
}

function tracknstack_embedded_player($node) {
  $output = '';
  
  $jplayer_path = variable_get('tracknstack_jplayer_path', null);

  if (!empty($jplayer_path) && $node->tracknstack->mixdown_fid) {
    $file = tracknstack_file_load($node->tracknstack->mixdown_fid);
    
    $mixdown_extension = tracknstack_get_filename_extension($file->filename);
    
    if (!in_array($mixdown_extension, array('wav', 'ogg'))) {
      return null;
    }
    
    if ($mixdown_extension == 'ogg') {
      $mixdown_extension = 'oga';
    }
    
    $file_url = file_create_url($file->filepath);
    $jplayer_url = tracknstack_get_base_url() . $jplayer_path;
    
    drupal_add_css($jplayer_path . '/jplayer.blue.monday.css');
    drupal_add_js('http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js');
    drupal_add_js($jplayer_path . '/jquery.jplayer.min.js');
    drupal_add_js('$(document).ready(function(){$("#jquery_jplayer_' . $node->nid . '").jPlayer({ready: function () {$(this).jPlayer("setMedia", {' . $mixdown_extension . ': "' . $file_url . '"})},ended: function (event) {$(this).jPlayer("play", 0);},swfPath: "' . $jplayer_url . '",supplied: "' . $mixdown_extension . '",cssSelectorAncestor: "#jp_interface_' . $node->nid . '"}).bind($.jPlayer.event.play, function() {$(this).jPlayer("pauseOthers");});});', 'inline');

    $output .= '<div class="jp-audio">';
    $output .= '<div class="jp-type-single">';
    $output .= '<div id="jquery_jplayer_' . $node->nid . '"></div>';
    $output .= '<div id="jp_interface_' . $node->nid . '" class="jp-gui jp-interface">';
    $output .= '<ul class="jp-controls">';
    $output .= '<li><a href="#" class="jp-play" tabindex="1">play</a></li>';
    $output .= '<li><a href="#" class="jp-pause" tabindex="1">pause</a></li>';
    $output .= '<li><a href="#" class="jp-stop" tabindex="1">stop</a></li>';
    $output .= '<li><a href="#" class="jp-mute" tabindex="1">mute</a></li>';
    $output .= '<li><a href="#" class="jp-unmute" tabindex="1">unmute</a></li>';
    $output .= '</ul>';
    $output .= '<div class="jp-progress">';
    $output .= '<div class="jp-seek-bar">';
    $output .= '<div class="jp-play-bar"></div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<div class="jp-volume-bar">';
    $output .= '<div class="jp-volume-bar-value"></div>';
    $output .= '</div>';
    $output .= '<div class="jp-time-holder">';
    $output .= '<div class="jp-current-time"></div>';
    $output .= '<div class="jp-duration"></div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<div id="jp_playlist_' . $node->nid . '" class="jp-playlist"></div>';
    $output .= '</div>';
    $output .= '</div>';
  }

  return $output;
}